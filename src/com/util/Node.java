package com.util;

import com.entities.Form;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Administrator on 10/2/2015.
 */
public class Node {

    private Integer index;
    private Integer parent_index = null;
    private ArrayList<Integer> child_id;
    private HashMap<Integer, ArrayList> identity = new HashMap<Integer, ArrayList>();
    // ArrayList<HashMap> progeny = new ArrayList<HashMap>();
    private Integer id;



    public Node (Integer id) {
        this.id = id;
        this.identity.put(id, new ArrayList<HashMap>());
    //    this.identity.put()
    }

    public Integer getIndex(){
        return this.index;
    }

    public void setIndex(Integer index){
        this.index = index;
    }

    public Integer getParentIndex(){
        return this.parent_index;
    }

    public void setParentIndex(Integer parent_index){
        this.parent_index = parent_index;
    }

    public ArrayList<Integer> getChildId(){
        return this.child_id;
    }

    public void setChildId(ArrayList<Integer> child_id){
        this.child_id = child_id;
    }

    public void addProgeny(HashMap child){
        this.identity.get(this.id).add(child);
    }

    public HashMap<Integer, ArrayList> getIdentity(){
        return this.identity;
    }

    public void print(){
        System.out.println("node index: " + index.toString());
        if (parent_index != null) {
            System.out.println("node parent index: "+ parent_index.toString());
        } else {
            System.out.println("node parent index: null");
        }
        System.out.println("node child id: "+ child_id.toString());
    }
}
