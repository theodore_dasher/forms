package com.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 * Created by Administrator on 9/25/2015.
 */
public class ConnectionConfiguration {

    public static Connection getConnection(String url, String user, String password) {
        Connection connection = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;
    }

    public static Connection getConnection() throws IOException {

        // get db.properties from file

        String file_path = "C:\\Users\\Administrator\\Desktop\\db.properties";
        Properties properties = PropertiesFile.getProperties(file_path);
        String url = properties.getProperty("url");
        String user = properties.getProperty("username");
        String password = properties.getProperty("password");

        // establish connection

        Connection connection = null;
        connection = ConnectionConfiguration.getConnection(url, user, password);

        return connection;
    }
}
