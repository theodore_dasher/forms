package com.util;

import com.daoimpl.FlatFormDaoImpl;
import com.entities.FlatForm;

import com.entities.Form;
import com.rits.cloning.Cloner;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Administrator on 10/2/2015.
 */

public class Step {

    private static Integer node_count = 0;
    private static Integer current_index = null;
    private static HashMap<Integer, Node> node_map = new HashMap<>();
    private static boolean more_nodes = true;

    public Step(int id) {

        // set instantiation context

        Node node = new Node(id);

        if (this.getClass() == Step.class) {
            if (node_count > 0) {
                node.setParentIndex(current_index);
                Node old_node = node_map.get(current_index);
                old_node.addProgeny(node.getIdentity());
            }
            node.setIndex(node_count);
            current_index = node.getIndex();
            node_count++;
        }

        // load child id vector

        FlatFormDaoImpl ffdi = new FlatFormDaoImpl();
        FlatForm flatform = ffdi.selectById(id);
        ArrayList<Integer> child_id = flatform.getChildId();
        System.out.println(id);
        System.out.println(child_id);
        ArrayList<Integer> list = new ArrayList<Integer>(child_id.size());
        for (int i : child_id) {
            if (i > 0) {
                list.add(i); // hack because null is read into int array as zero
            }
        }
        node.setChildId(list);
        node_map.put(node.getIndex(), node);

        Form form = flatform.convertToForm();
        form.getDescription();
        form.getLabel();


    }

    public static void takeStep() {

        if (!node_map.isEmpty()){
            Node node = node_map.get(current_index);
            if(node.getChildId().size() != 0) {
                int next_id = node.getChildId().get(0);
                node.getChildId().remove(0);
                node_map.put(node.getIndex(), node);
                new Step(next_id);
            } else if (node.getParentIndex() != null){
                current_index = node.getParentIndex();
            } else {
                more_nodes = false;
            }
        }
    }

    public static void print() {
        System.out.println("node count: " + node_count);

        Node node = node_map.get(current_index);
        node.print();

        if (more_nodes == false) {
            System.out.println("no more nodes to crawl");
        }

        System.out.println("-----------------------");
    }

    public static boolean isCrawling() {
        return more_nodes;
    }

    public static HashMap getTree(){

        Node first_node = node_map.get(0);
        Cloner cloner = new Cloner();
        HashMap clone = cloner.deepClone(first_node.getIdentity());
        return clone;

    }

}
