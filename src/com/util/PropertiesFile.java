package com.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Administrator on 9/25/2015.
 */
public class PropertiesFile {
    public static Properties getProperties(String file_path) throws IOException {
        Properties properties = new Properties();
        FileInputStream in = null;

        try {
            in = new FileInputStream(file_path);
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                in.close();
            }
        }

        return properties;
    }
}
