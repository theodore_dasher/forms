package com.entities;

import com.daoimpl.FormDaoImpl;

import java.util.ArrayList;

/**
 * Created by Administrator on 9/25/2015.
 */
public class FlatForm {

    private int id;
    private ArrayList<Integer> child_id;

    public FlatForm() {

    }

    public FlatForm(int id, ArrayList<Integer> child_id) {
        setId(id);
        setChildId(child_id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {

        // check for cycles

        if (this.child_id != null) {
            for (int i = 0; i < this.child_id.size(); i++) {
                if (id == this.child_id.get(i)){
                    throw new RuntimeException("form tree cannot point to itself");
                }
            }
        }

        this.id = id;
    }

    public ArrayList<Integer> getChildId() {
        return child_id;
    }

    public void setChildId(ArrayList<Integer> child_id) {

        // check for cycles
        if (child_id != null){
            for (int i = 0; i < child_id.size(); i++) {
                if (this.id == child_id.get(i)) {
                    throw new RuntimeException("form tree cannot point to itself");
                }
            }
        }
        this.child_id = child_id;
    }

    public Form convertToForm() {
        FormDaoImpl fdi = new FormDaoImpl();
        Form form = fdi.selectById(this.id);
        return form;
    }

}
