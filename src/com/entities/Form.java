package com.entities;

import java.util.ArrayList;

/**
 * Created by Administrator on 9/25/2015.
 */
public class Form {

    private int id;
    private String label;
    private String description;
 //   private ArrayList<Integer> child_id;

    public Form() {

    }

    public Form(String label, String description, ArrayList<Integer> child_id) {
        this.label = label;
        this.description = description;
    //    this.child_id = child_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

 //   public ArrayList<Integer> getIdVector() {
 //       return child_id;
 //   }

//    public void setChildId(ArrayList<Integer> child_id) {
//        this.child_id = child_id;
//    }

//    public FlatForm convertToFlatForm() {
//
 //       FlatForm flatform = new FlatForm(this.id, this.child_id);
//
 //       return flatform;
 //   }
}
