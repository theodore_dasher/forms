package com.dao;

import com.entities.FlatForm;
import java.util.List;

/**
 * Created by Administrator on 9/29/2015.
 */
public interface FlatFormDao {

    void createFlatFormTable();

    void insert(FlatForm flatform);

    FlatForm selectById(int id);

    void delete(int id);

    void update(FlatForm flatform);
}
