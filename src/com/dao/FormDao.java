package com.dao;

import com.entities.Form;
import java.util.List;

/**
 * Created by Administrator on 9/29/2015.
 */
public interface FormDao {

    void createFormTable();

    int insert(Form form);

    Form selectById(int id);

    List<Form> selectAll();

    void delete(int id);

    void update(Form form, int id);
}
