package com.daoimpl;

import com.dao.FlatFormDao;
import com.entities.FlatForm;
import com.entities.Form;
import com.mysql.jdbc.*;
import com.util.ConnectionConfiguration;

import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 9/29/2015.
 * Used mainly for retrieval
 */
public class FlatFormDaoImpl implements FlatFormDao {


    @Override
    public void createFlatFormTable() {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS flatform (id int," +
                    "child_id int, child_index int," +
                    "FOREIGN KEY (id) REFERENCES form(id) ON UPDATE CASCADE ON DELETE CASCADE," +
                    "FOREIGN KEY (child_id) REFERENCES form(id) ON UPDATE CASCADE ON DELETE CASCADE)");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if(statement != null ){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void insert(FlatForm flatform) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionConfiguration.getConnection();

            // delete existing rows with this id

            preparedStatement = connection.prepareStatement("DELETE FROM flatform " +
                    "WHERE id = ?");
            preparedStatement.setInt(1, flatform.getId());

            // insert flatform into flatform table

            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement("INSERT INTO flatform" +
                    " (id, child_id, child_index) VALUES (?, ?, ?)");

            ArrayList<Integer> child_id = flatform.getChildId();

            if (child_id == null) {
                preparedStatement.setInt(1, flatform.getId());
                preparedStatement.setNull(2, Types.INTEGER);
                preparedStatement.setNull(3, Types.INTEGER);
                preparedStatement.addBatch();
            } else {

                for (int idx = 0; idx < child_id.size(); idx++) {
                    preparedStatement.setInt(1, flatform.getId());
                    preparedStatement.setInt(2, child_id.get(idx));
                    preparedStatement.setInt(3, idx);
                    preparedStatement.addBatch();
                }
            }

            preparedStatement.executeBatch();
            connection.commit();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public FlatForm selectById(int id) {
        FlatForm flatform = new FlatForm();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("SELECT child_id, child_index" +
                    " FROM flatform WHERE id = ?");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            // retrieve flatform patameters

            if(!resultSet.next()){
                throw new Exception("no data found for id " + id + " in table flatform");
            } else{
                HashMap<Integer, Integer> map_child_id = new HashMap<Integer, Integer>();

                do{
                    map_child_id.put(resultSet.getInt("child_index"),
                            resultSet.getInt("child_id"));
                }
                while(resultSet.next());

                int n_rows = map_child_id.size();
                ArrayList<Integer> child_id = new ArrayList<Integer>(n_rows);
                for(HashMap.Entry<Integer, Integer> entry : map_child_id.entrySet()){
                    child_id.add(entry.getKey(), entry.getValue());
                }

                flatform.setId(id);
                flatform.setChildId(child_id);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return flatform;
    }

    @Override
    public void delete(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            PreparedStatement statement = connection.prepareStatement("DELETE FROM" +
                    " flatform WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void update(FlatForm flatform) {
        delete(flatform.getId());
        insert(flatform);
    }
}
