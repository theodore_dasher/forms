package com.daoimpl;

import com.dao.FormDao;
import com.entities.Form;
import com.mysql.jdbc.*;
import com.util.ConnectionConfiguration;

import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 9/29/2015.
 */
public class FormDaoImpl implements FormDao {


    @Override
    public void createFormTable() {
        Connection connection = null;
        Statement statement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            statement.execute("CREATE TABLE IF NOT EXISTS form (id int primary key unique auto_increment," +
                    "label varchar(45), description longtext)");

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if(statement != null ){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public int insert(Form form) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int new_id = 0;

        try {
            connection = ConnectionConfiguration.getConnection();

            // insert into form table

            preparedStatement = connection.prepareStatement("INSERT INTO form (label, description)" +
                    "VALUES (?, ?)");
            preparedStatement.setString(1, form.getLabel());
            preparedStatement.setString(2, form.getDescription());
            preparedStatement.executeUpdate();

            // return id

            preparedStatement = connection.prepareStatement("SELECT LAST_INSERT_ID()");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                new_id = resultSet.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return new_id;

    }

    @Override
    public Form selectById(int id) {
        Form form = new Form();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            preparedStatement = connection.prepareStatement("SELECT * FROM form WHERE id = ?");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()) {
                form.setId(resultSet.getInt("id"));
                form.setLabel(resultSet.getString("label"));
                form.setDescription(resultSet.getString("description"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return form;
    }

    @Override
    public List<Form> selectAll() {
        List<Form> forms = new ArrayList<Form>();
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery("SELECT * FROM form");
            while(resultSet.next()) {
                Form form = new Form();
                form.setId(resultSet.getInt("id"));
                form.setLabel(resultSet.getString("label"));
                form.setDescription(resultSet.getString("description"));

                forms.add(form);
            }
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if(resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if(statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return forms;
    }

    @Override
    public void delete(int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            PreparedStatement statement = connection.prepareStatement("DELETE FROM" +
                    " form WHERE id = ?");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void update(Form form, int id) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            connection = ConnectionConfiguration.getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE form SET" +
                    " label  = ?, description = ? WHERE id = ?");
            preparedStatement.setString(1, form.getLabel());
            preparedStatement.setString(2, form.getDescription());
            preparedStatement.setInt(3, id);
            preparedStatement.executeUpdate();

        } catch (Exception e){
            e.printStackTrace();
        } finally {
            if(preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if(connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
