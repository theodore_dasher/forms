package com.servicesimpl;

import com.daoimpl.FlatFormDaoImpl;
import com.daoimpl.FormDaoImpl;
import com.entities.FlatForm;
import com.entities.Form;
import com.services.FormServices;
import com.util.Step;

import java.util.ArrayList;

/**
 * Created by Administrator on 9/30/2015.
 */
public class FormServicesImpl implements FormServices {

    private FormDaoImpl fdi = new FormDaoImpl();
    private FlatFormDaoImpl ffdi = new FlatFormDaoImpl();

    public FormServicesImpl() {
        fdi.createFormTable();
        ffdi.createFlatFormTable();
    }

    @Override
    public void createForm(String label, String description, ArrayList<Integer> child_id) {

        // create form table if it does not exist

        FormDaoImpl fdi = new FormDaoImpl();
        fdi.createFormTable();

        // create new form instance and update form table

        Form form = new Form(label, description, child_id);
        int new_id = fdi.insert(form);
        form.setId(new_id);

        // create flatform table if it does not exits

        FlatFormDaoImpl ffdi = new FlatFormDaoImpl();
        ffdi.createFlatFormTable();

        // create new flatform and update flatform table

        FlatForm flatform = new FlatForm(form.getId(), child_id);
        ffdi.insert(flatform);

        // commandline UI

        System.out.println("form #" + new_id + " has been successfully created");

    }




}
